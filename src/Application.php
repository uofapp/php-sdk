<?php

namespace Uofuu\SDK;

/**
 * Class Application
 *
 * @property \Uofuu\SDK\Payment $payment
 * @property \Uofuu\SDK\Wechat  $wechat
 * @package Uofuu\SDK
 */
class Application
{
    protected $http_client;

    public function __construct($client_id, $client_secret, $service_host = '')
    {
        Config::instance($client_id, $client_secret);
        Http::instance($service_host);
    }

    function __get($name)
    {
        if (!in_array($name, get_class_vars(__CLASS__))) {
            $class = __NAMESPACE__ . '\\' . ucfirst($name);

            return new $class;
        }
    }

    function setSandbox($sandbox = true)
    {
        Config::setSandbox($sandbox);
    }
}