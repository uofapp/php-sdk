<?php

namespace Uofuu\SDK;

class Config
{
    static $client_id, $client_secret;
    static             $sandbox = false;

    public static function instance($app_key, $app_secret)
    {
        self::$client_id     = $app_key;
        self::$client_secret = $app_secret;
    }

    public static function setSandbox($sandbox)
    {
        self::$sandbox = $sandbox;
    }

    public static function getSandbox()
    {
        return self::$sandbox;
    }

}