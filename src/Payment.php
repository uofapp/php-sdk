<?php

namespace Uofuu\SDK;

class Payment
{
    public function prepay(array $body)
    {
        $response = Http::request('POST', '/api/unified_order', [
            'form_params' => $body,
        ]);

        return $response;
    }

    public function query($out_trade_no)
    {
        $response = Http::request('GET', '/api/query_order', [
            'query' => [
                'out_trade_no' => $out_trade_no,
            ],
        ]);

        return $response;
    }

}