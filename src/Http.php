<?php

namespace Uofuu\SDK;

use GuzzleHttp\Client;

class Http
{
    /** @var Client */
    static $client;
    static $access_token;

    public static function instance($service_host = '')
    {
        self::$client = new Client([
            'base_uri' => (rtrim($service_host, '/') ?: 'https://uofuu.com'),
            'headers'  => self::defaultHeaders(),
        ]);

        self::refreshAccessToken();
    }

    public static function defaultHeaders()
    {
        return [
            'x-app-key'    => Config::$client_id,
            'Content-Type' => 'application/json',
        ];
    }

    public static function refreshAccessToken($scope = '')
    {
        $response = self::$client->post('/oauth/token', [
            'form_params' => [
                'grant_type'    => 'client_credentials',
                'client_id'     => Config::$client_id,
                'client_secret' => Config::$client_secret,
                'scope'         => $scope,
            ],
        ]);

        $json               = $response->getBody()->getContents();
        self::$access_token = json_decode($json, true);

        return self::$access_token;
    }

    public static function getAccessToken()
    {
        return self::$access_token;
    }

    public static function setAccessToken($access_token)
    {
        self::$access_token = $access_token;
    }

    static function request($method, $path = '', $options = [])
    {
        $timestamp = time();

        $headers = array_key_exists('headers', $options) ? $options['headers'] : [];

        $options['headers'] = array_merge([
            'x-sandbox'     => Config::getSandbox(),
            'x-timestamp'   => $timestamp,
            'Accept'        => 'application/json',
            'Authorization' => trim(self::$access_token['token_type'] . ' ' . self::$access_token['access_token']),
        ], $headers);

        try {
            $response = self::$client->request($method, $path, $options);

            return $response->getBody()->getContents();
        } catch (\GuzzleHttp\Exception\GuzzleException $exception) {
            return $exception->getMessage();
        }
    }
}