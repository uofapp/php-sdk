<?php

namespace Uofuu\SDK;

class Wechat
{
    public function jssdkConfig($js_api_list = ['chooseWXPay'], $current_url = '', $debug = false)
    {
        $response = Http::request('GET', '/api/jssdk_config', [
            'query' => [
                'js_api_list' => $js_api_list,
                'debug'       => $debug,
                'url'         => $current_url ?: $this->guessCurrentUrl(),
            ],
        ]);

        return $response;
    }

    function guessCurrentUrl()
    {
        if (PHP_SAPI == 'cli') {
            return 'http://' . gethostbyname(gethostname());
        }

        if ($this->isAjax()) {
            return $_SERVER['HTTP_REFERER'];
        }

        $server_protocol = $_SERVER['SERVER_PORT'] == 443 ? 'https://' : 'http://';
        $http_host       = $_SERVER['HTTP_HOST'];
        $request_uri     = $_SERVER['REQUEST_URI'];

        return $server_protocol . $http_host . $request_uri;
    }

    function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'));
    }
}